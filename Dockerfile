#docker build -t influans/teeworlds:0.6.3 .

FROM debian:jessie

ENV TEE_GAMETYPE ctf
ENV TEE_NAME Teeworld
ENV PASSWORD influans2016

RUN apt-get update \
 && apt-get install -y teeworlds-server pwgen \
 && apt-get clean all \
 && rm -fr /var/lib/apt/lists/*

ENV TINI_VERSION v0.13.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini

COPY data/maps/* /usr/share/games/teeworlds/data/maps/
COPY *.cfg /
COPY run.sh /run.sh

RUN chmod +x /run.sh

EXPOSE 8303/udp

ENTRYPOINT ["/tini", "-g", "--"]
CMD ["/run.sh"]
