#!/bin/sh

echo "sv_rcon_password $(pwgen -s 12 1)" >> /${TEE_GAMETYPE}.cfg
echo "sv_name $TEE_NAME" >> /${TEE_GAMETYPE}.cfg
echo "password $PASSWORD" >> /${TEE_GAMETYPE}.cfg
grep password /${TEE_GAMETYPE}.cfg
if [ "$(grep -c Alpine /etc/issue)" = "1" ]; then
  /usr/bin/teeworlds_srv -f /${TEE_GAMETYPE}.cfg
else
  exec /usr/games/teeworlds-server -f /${TEE_GAMETYPE}.cfg
fi
