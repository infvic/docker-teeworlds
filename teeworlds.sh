#!/bin/bash
. $HOME/.bash_aliases
docker stop teeworlds && docker rm teeworlds
docker run --name teeworlds -p 8303:8303/udp -e TEE_NAME="teeworlds@$HOSTNAME by vic" -d influans/teeworlds:0.6.3-alpine
docker exec -it teeworlds more /ctf.cfg | grep password

